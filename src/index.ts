import amqplib from 'amqplib';
import puppeteer from 'puppeteer';

async function pupputer(url: string) {
  const browser = await puppeteer.launch();
  const page = await browser.newPage();

  await page.goto(url);

  const cookies = await page.cookies();
  const size = page.viewport();
  //Ширина и высота содержимого страницы
  const elem = await page.$('.question');
  if (elem !== null) {
    const boundingBox = await elem.boundingBox();
    console.log('HEIGHT =', boundingBox?.height, 'WIDHT =', boundingBox?.width);
  }
  console.log('COOKIES =', cookies, '\nWINDOW SIZE =', size);
}

async function main() {
  const queue = 'URLs';
  let env = process.env.RABBITMQ_ADDRESS;
  if (!env) {
    console.log('NO ENV, RABBITMQ_ADDRESS');
  }

  amqplib.connect(env as string, function (error0: any, connection: any) {
    if (error0) {
      throw error0;
    }
    connection.createChannel(function (error1: any, channel: any) {
      if (error1) {
        throw error1;
      }

      channel.assertQueue(queue, {
        durable: false,
      });

      console.log(' [*] Waiting for messages in %s. To exit press CTRL+C', queue);

      channel.consume(queue, async (msg: any) => {
        if (msg !== null && msg.content !== null) {
          pupputer(msg.content.toString());
        } else {
          console.log('Consumer cancelled by server');
        }
      });
    });
  });
}

// pupputer('https://stackoverflow.com/questions/55225525/how-to-draw-a-bounding-box-on-an-element-with-puppeteer');
main();
